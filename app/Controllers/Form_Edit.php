<?php

namespace App\Controllers;

use App\Models\FormEditModel;

class form_edit extends BaseController
{
    protected $FormEditModel;

    public function __construct()
    {
        $this->FormEditModel = new FormEditModel();
    }
    public function index()
    {
        $transaksi = $this->FormEditModel->findAllData();

        $data = [
            'title' => 'Barang',
            'transaksi' => $transaksi
        ];


        return view('pages/form_edit', $data);
    }
}
