<?php

namespace App\Controllers;

use App\Models\TransaksiModel;

class rejected_user extends BaseController
{
    protected $TransaksiModel;

    public function __construct()
    {
        $this->TransaksiModel = new TransaksiModel();
    }
    public function index()
    {
        $transaksi = $this->TransaksiModel->findAllData();

        $data = [
            'title' => 'Barang',
            'transaksi' => $transaksi
        ];


        return view('pages/rejected_user', $data);
    }
}
