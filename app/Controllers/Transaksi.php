<?php

namespace App\Controllers;

use App\Models\RequestModel;

class Transaksi extends BaseController
{
    protected $RequestModel;

    public function __construct()
    {
        $this->RequestModel = new RequestModel();
    }
    public function index()
    {
        $Request = $this->RequestModel->findAll();
        // $Request = $this->RequestModel->findAll();

        $data = [
            'title' => 'Request',
            'Request' => $Request
        ];

        return view('pengajuan/pengajuan', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Tambah Barang'
        ];

        return view('pages/form_addbarang', $data);
    }

    public function save()
    {
        $this->RequestModel->save([
            'nama_barang' => $this->RequestModel->getVar('nama_barang'),
            'jumlah_permintaan' => $this->RequestModel->getVar('jumlah_permintaan'),
            'tanggal_permintaan' => $this->RequestModel->getVar('tanggal_permintaan')
        ]);

        return redirect()->to('/pengajuan');
    }
}