<?php

namespace App\Controllers;

use App\Models\UserModel;

class Auth extends BaseController
{
  protected $userModel;
  protected $session;

  public function __construct()
  {
    $this->userModel = new UserModel();
    $this->session = session();
  }


  public function index()
  {
    echo ("testing in index ");
  }

  public function login()
  {
    // get data from the form
    // as username and password
    $username = $this->request->getPost('username');
    $password = $this->request->getPost('password');

    if ($username != null && $password != null) {
      // start login and make session

      // get data user just one call
      $currentUser = $this->userModel->where('username', $username)->where('password', $password)->first();

      if ($currentUser != null) {

        // sefine data want to save as session
        $sessionData = [
          'id_user' => $currentUser['id_user'],
          'username' => $currentUser['username'],
          'role' => $currentUser['role'],
        ];

        // start make session
        $this->session->set($sessionData);

        // redirect user when 
        if ($currentUser["role"] == "Admin") {
          // bring to beranda admin
          redirect()->to(site_url('/beranda_admin'));
        } else {
          redirect()->to(site_url('/'));
        }
      } else {
        // if not 
        // bring back to form
        echo view('pages/login');
      }
    }

    // if not 
    // bring back to form
    echo view('pages/login');
  }
}
