<?php

namespace App\Controllers;

use App\Models\UserModel;

class editpro extends BaseController
{
    protected $UserModel;

    public function __construct()
    {
        $this->UserModel = new UserModel();
    }

    public function index()
    {
        $user = $this->UserModel->where('id_user', '1')->findAll();

        $data = [
            'title' => 'Edit Profil',
            'user' => $user
        ];

        return view('pages/editpro', $data);
    }
}
