<?php

namespace App\Controllers;

use App\Models\form_reviewModel;

class form_review extends BaseController
{
    protected $form_reviewModel;

    public function __construct()
    {
        $this->form_reviewModel = new form_reviewModel();
    }
    public function index()
    {
        $transaksi = $this->form_reviewModel->findAllData();

        $data = [
            'title' => 'Barang',
            'transaksi' => $transaksi
        ];


        return view('pages/form_review', $data);
    }
}
