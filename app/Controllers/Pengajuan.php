<?php

namespace App\Controllers;

use App\Models\BarangModel;
use App\Models\RequestModel;
use App\Models\TransaksiModel;

class Pengajuan extends BaseController
{
  protected $requestModel;
  protected $transactionModel;
  protected $barangModel;

  public function __construct()
  {
    $this->requestModel = new RequestModel();
    $this->transactionModel = new TransaksiModel();
    $this->barangModel = new BarangModel();
  }

  /**
   * laad data from the index page for 
   * Pengajuan page 
   */
  public function index()
  {
    // get data from request database
    // for all data
    $request = $this->requestModel->findAll();

    // define all data you want to show and get it
    $data = [
      'title' => 'Request',
      'request' => $request,
    ];

    // return all this data into the 
    // pengajuan page
    return view('pengajuan/pengajuan', $data);
  }


  /**
   * use to add data from the form
   * and save data for request
   */
  public function addRequest()
  {

    // validate data before we're going to use and save data
    // of course we want to ensure all data save
    $validation =  \Config\Services::validation();
    $validation->setRules(['nama_barang' => 'required']);
    $isDataValid = $validation->withRequest($this->request)->run();

    // if data input valid
    // we're try to save data 
    if ($isDataValid) {
      $this->requestModel->insert([
        "nama_barang" => $this->request . getGet("nama_barang"),
        "jumlah_permintaan" => $this->request . getPost("jumlah_permintaan"),
        "id_barang" => $this->request . getPost("id_barang"),
        "tanggal_permintaan" => $this->request . getPost("tanggal_permintaan"),
        "id_user" => $this->request . getPost("id_user")
      ]);

      // after save data finish
      // bring it all into the list pengajuan
      return redirect('pengajuan');
    }

    // opps, something error found
    // send to form create request again
    $data = [
      "title" => "Add Barang",
    ];


    return view('pages/form_addbarang', $data);
  }

  /**
   * edit the specify request
   * we all need the data to update and 
   */
  public function editRequest($requestId)
  {
    // get all data we're try to update about
    $data = [
      "request" => $this->requestModel->where('id_request', $requestId)->first(),
    ];

    // validate it
    $validation =  \Config\Services::validation();
    $validation->setRules([
      'id_request' => 'required',
      'nama_barang' => 'required'
    ]);
    $isDataValid = $validation->withRequest($this->request)->run();
    if ($isDataValid) {
      $this->requestModel->update($requestId, [
        "nama_barang" => $this->request . getPost("nama_barang"),
        "jumlah_permintaan" => $this->request . getPost("jumlah_permintaan"),
        "id_barang" => $this->request . getPost("id_barang"),
        "tanggal_permintaan" => $this->request . getPost("tanggal_permintaan"),
        "id_user" => $this->request . getPost("id_user")
      ]);

      // now after finish to save data 
      // we're going to back into the editing form
      return redirect("/edit_form");
    }

    // opps, something trouble found
    // bring back to form
    return view("pages/form_addbarang", $data);
  }
}
