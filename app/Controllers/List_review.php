<?php

namespace App\Controllers;

use App\Models\BarangModel;

class List_review extends BaseController
{
    protected $BarangModel;

    public function __construct()
    {
        $this->BarangModel = new BarangModel();
    }
    public function index()
    {
        $Barang = $this->BarangModel->findAll();

        $data = [
            'title' => 'Daftar Barang',
            'Barang' => $Barang
        ];

        // $BarangModel = new \App\Models\BarangModel();
        // $BarangModel = new BarangModel();



        return view('review/review', $data);
    }

    public function detail($slug)
    {
        echo $slug;
    }
}
