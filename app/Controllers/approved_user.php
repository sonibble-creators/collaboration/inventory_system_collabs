<?php

namespace App\Controllers;

use App\Models\TransaksiModel;

class approved_user extends BaseController
{
  protected $TransaksiModel;

  public function __construct()
  {
    $this->TransaksiModel = new TransaksiModel();
  }
  public function index()
  {
    $transaksi = $this->ransaksiModel->findAllData();

    $data = [
      'title' => 'Barang',
      'transaksi' => $transaksi
    ];


    return view('pages/approved_user', $data);
  }
}
