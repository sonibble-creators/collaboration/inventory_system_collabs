<?php

namespace App\Controllers;

class Pages extends BaseController
{
  public function index()
  {
    $data = [
      'title' => 'Beranda'
    ];
    echo view('pages/beranda', $data);
  }

  public function pengajuan()
  {
    $data = [
      'title' => 'Pengajuan'
    ];
    echo view('pengajuan/pengajuan', $data);
  }

  public function beranda_admin()
  {
    $data = [
      'title' => 'beranda_admin'
    ];
    echo view('pages/beranda_admin', $data);
  }

  public function list_review_user()
  {
    $data = [
      'title' => 'list_review_user'
    ];
    echo view('pages/list_review_user', $data);
  }

  public function data_user()
  {
    $data = [
      'title' => 'data_user'
    ];
    echo view('pages/data_user', $data);
  }

  public function editpro()
  {
    $data = [
      'title' => 'editpro'
    ];
    echo view('pages/editpro', $data);
  }

  public function login()
  {
    $data = [
      'title' => 'login'
    ];
    echo view('pages/login', $data);
  }

  public function register()
  {
    $data = [
      'title' => 'register'
    ];
    echo view('pages/register', $data);
  }

  // public function form_review()
  // {
  //     $data = [
  //         'title' => 'form_review'
  //     ];
  //     echo view('review_admin/form_review', $data);
  // }

  public function landing_page()
  {
    echo view('pages/landing_page');
    $data = [
      'title' => 'landing_page'
    ];
    echo view('pages/landing_page', $data);
  }

  public function list_review()
  {
    $data = [
      'title' => 'list_review'
    ];
    echo view('pages/list_review', $data);
  }

  public function list_approved()
  {
    $data = [
      'title' => 'list_approved'
    ];
    echo view('pages/list_approved', $data);
  }

  public function list_rejected()
  {
    $data = [
      'title' => 'list_rejected'
    ];
    echo view('pages/list_rejected', $data);
  }

  public function list_revised()
  {
    $data = [
      'title' => 'list_revised'
    ];
    echo view('pages/list_revised', $data);
  }

  public function form_edit()
  {
    $data = [
      'title' => 'form_edit'
    ];
    echo view('pages/form_edit', $data);
  }

  public function approved_user()
  {
    $data = [
      'title' => 'approved_user'
    ];
    echo view('pages/approved_user', $data);
  }

  public function rejected_user()
  {
    $data = [
      'title' => 'rejected_user'
    ];
    echo view('pages/rejected_user', $data);
  }

  public function approved_revised_user()
  {
    $data = [
      'title' => 'approved_revised_user'
    ];
    echo view('pages/approved_revised_user', $data);
  }

  public function waiting_user()
  {
    $data = [
      'title' => 'waiting_user'
    ];
    echo view('pages/waiting_user', $data);
  }


  /**
   * ## addBarang
   * 
   * this function allow user to add
   * and products
   */
  public function addbarang()


  {
    $data = [
      'title' => 'tambah_barang'
    ];
    echo view('pages/form_addbarang', $data);
  }
}
