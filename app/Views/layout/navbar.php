<nav class="navbar navbar-expand-lg navbar-dark bg-custom-color">
    <div class="container">
        <img src="img/Lambang_Kab_Bangli.png" alt="" width="70" height="auto" class="d-inline-block align-text-top me-3">
        </a>
        <span class="navbar-brand mb-0 h1">Inventaris Barang</span>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="container">
                <div class="navbar-nav navbar-nav-custom">
                    <a class="nav-link active me-3" aria-current="page" href="/">Beranda</a>
                    <a class="nav-link me-3" href="/pengajuan">Pengajuan</a>
                    <a class="nav-link me-3" href="/review_user">Review</a>
                    <a class="nav-link" href="/editpro">Edit Profil</a>
                </div>
            </div>
        </div>
    </div>
</nav>