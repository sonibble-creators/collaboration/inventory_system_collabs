<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container mt-3 custom-margin">
  <div class="row">
    <div class="col-7 col-md-4">
      <div class="card" style="width: 20rem;">
        <div class="card-header text-white bg-btn">
          <i class="bi bi-person-circle position-absolute" style="font-size: 1.5rem;"></i>
          <p class="ps-4 ms-2 pt-2">Informasi User</p>
        </div>
        <img src="/img/profil.png" class="card-img-top w-75 h-auto mx-auto d-block pt-3" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title">Username</h5>
          <p class="card-text">username@gmail.com</p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Nama : Ni Wayan Abcd</li>
          <li class="list-group-item">NIP : 12345678</li>
          <li class="list-group-item">Instansi : Kominfosan</li>
          <li class="list-group-item">Telp/Hp : 08311234567</li>
        </ul>
      </div>
    </div>
    <div class="col-md-8 border border-dark fw-bold pt-4">
      <div class="pt-5">
        <i class="bi bi-cart-fill position-absolute ps-4" style="font-size: 1.5rem;"></i>
        <h6 class="pt-2 ms-5 fw-bold">Input Barang</h6>
      </div>
      <div class="container">
        <table class="table table-striped table-bordered table-hover mt-3">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama Barang</th>
              <th scope="col">Jumlah Permintaan</th>
              <th scope="col">Tanggal Permintaan</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1 ?>
            <?php foreach ($request as $r) : ?>
              <tr>
                <th scope="row"><?= $i++; ?> </th>
                <td><?= $r['nama_barang']; ?></td>
                <td><?= $r['jumlah_permintaan']; ?></td>
                <td><?= $r['tanggal_permintaan']; ?></td>
                <td>
                  <a href=""><i class="bi bi-trash3-fill"></i></a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <div class="d-grid col-2 mx-auto pb-3">
        <a href="/pengajuan/tambah"><button type="button" class="btn btn-outline-primary btn-sm rounded-pill text-dark fw-bold "><i class="bi bi-plus-circle-fill pe-1"></i>Baris Baru</button></a>
      </div>
      <div class="btn-toolbar pt-4 mt-5 justify-content-end" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group me-4" role="group" aria-label="Second group">
          <a href="/list_review"><button type="submit" class="btn btn-success rounded-pill fw-bold">Submit</button></a>
        </div>
      </div>
    </div>
  </div>
</div>

<?= $this->endSection(); ?>