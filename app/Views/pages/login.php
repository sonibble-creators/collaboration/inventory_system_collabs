<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
  <script src="https://kit.fontawesome.com/d10c4eb8a0.js" crossorigin="anonymous"></script>
  <title>Review Data Barang</title>
</head>

<body>
  <div class="cover" style="background-image:url(img/bg-login.jpg);
    background-size: cover;
    height: 100vh;">
    <div class="container">
      <div class="row justify-content-center mt-5 pt-5">
        <div class="col-md-4">
          <div class="card w-20rem card-login">
            <div class="card-header bg-transparent mb-0">
              <h3 class="text-center text-light">Login</h3>
            </div>
            <div class="card-body">
              <div class="card-body">
                <form action="<?php site_url('/login') ?>" method="post">
                  <div class="row mb-3">
                    <div class="col-sm-12">
                      <i class="bi bi-person-circle position-absolute ps-2" style="font-size: 1.5rem;"></i>
                      <input type="text" name="username" class="form-control ps-5" id="inputUsername" placeholder="username">
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-sm-12 icon">
                      <i class="bi bi-lock-fill position-absolute  ps-2" style="font-size: 1.5rem;"></i>
                      <input type="password" name="password" class="form-control ps-5" id="inputPassword" placeholder="password">
                    </div>
                  </div>
                  <div class="text-end">
                    <button type="submit" class="btn btn-primary rounded-pill bg-btn-auth">Login</button>
                  </div>
                  <div class="text-center border-top border-secondary mt-3 text-light">Belum punya akun?
                    <a href="/register" class="text-light">Daftar!</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Optional JavaScript; choose one of the two! -->
  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
</body>

</html>