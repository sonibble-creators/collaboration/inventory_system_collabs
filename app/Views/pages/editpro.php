<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container mt-5 pt-5 custom-margin">
    <div class="row">
        <div class="col-7 col-md-4">
            <div class="card m-3 p-5" style="width: 20rem;">
                <img src="/img/profil.png" class="card-img-top w-50 h-auto mx-auto d-block" alt="...">
                <div class="card-body text-center">
                    <?php foreach ($user as $U) : ?>
                        <h5 class="card-title"><?= $U['username']; ?></h5>
                        <p class="card-text"><?= $U['email']; ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-8 border fw-bold fs-5">Profil Setting
            <div class="container">
                <div class="row row-cols-2 pt-3">
                    <div class="col pb-3">
                        <div class="form-floating">
                            <input type="email" class="form-control" id="floatingInputGrid" placeholder="Name" value="<?= $U['nama']; ?>">
                            <label for="floatingInputGrid">Nama</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-floating">
                            <input type="email" class="form-control" id="floatingInputGrid" placeholder="name@example.com" value="<?= $U['email']; ?>">
                            <label for="floatingInputGrid">Email</label>
                            <div class="row g-2">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-floating">
                            <select class="form-select" id="floatingSelectGrid" aria-label="Floating label select example">
                                <option selected>Perempuan</option>
                                <option value="1">Laki-Laki</option>
                            </select>
                            <label for="floatingSelectGrid">Jenis Kelamin</label>
                        </div>
                    </div>
                    <div class="col pb-3">
                        <div class="form-floating">

                            <input type="email" class="form-control" id="floatingInputGrid" placeholder="NIP" value="<?= $U['NIP']; ?>">
                            <label for="floatingInputGrid">NIP</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-floating">

                            <select class="form-select" id="floatingSelectGrid" aria-label="Floating label select example">
                                <option selected>Kominfosan</option>
                                <option value="1">Kantor Pelayanan Perizinan Terpadu</option>
                                <option value="2">Badan Keuangan, Pendapatan dan Aset Daerah</option>
                                <option value="3">Badan Kepegawaian Daerah dan Pengembangan SDM</option>
                            </select>
                            <label for="floatingSelectGrid">Instansi</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-floating">

                            <input type="email" class="form-control" id="floatingInputGrid" placeholder="Telepon" value="<?= $U['no_telp']; ?>">
                            <label for="floatingInputGrid">No.Telp</label>
                        </div>
                    </div>
                    <div class="col-md-12 text-end p-3">
                        <button class="btn bg-btn rounded-pill text-light" type="button">Save Profil</button>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>