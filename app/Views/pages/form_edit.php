<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container mt-3 custom-margin">
    <div class="row">
        <div class="col-7 col-md-4">
            <div class="card" style="width: 20rem;">
                <div class="card-header text-white bg-btn">
                    <i class="bi bi-person-circle position-absolute" style="font-size: 1.5rem;"></i>
                    <p class="ps-4 ms-2 pt-2">Informasi User</p>
                </div>
                <img src="/img/profil.png" class="card-img-top w-75 h-auto mx-auto d-block pt-3" alt="...">
                <div class="card-body text-center">
                    <h5 class="card-title">Username</h5>
                    <p class="card-text">username@gmail.com</p>
                </div>
            </div>
        </div>
        <div class="col-md-8 border border-dark fw-bold pt-4">
            <div class="pt-5">
                <i class="bi bi-cart-fill position-absolute ps-4" style="font-size: 1.5rem;"></i>
                <h6 class="pt-2 ms-5 fw-bold">Edit Barang</h6>
            </div>
            <div class="container">
                <table class="table table-striped table-bordered table-hover mt-3" style="font-size: 15px">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Jumlah Permintaan</th>
                            <th scope="col">Tanggal Persetujuan</th>
                            <th scope="col">Harga</th>
                            <th scope="col">Sub Total</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1 ?>
                        <?php $array_harga = array('') ?>
                        <?php foreach ($transaksi as $t) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?> </th>
                                <td><?= $t['nama_barang']; ?></td>
                                <td><?= $t['jumlah_permintaan']; ?></td>
                                <td><?= $t['tgl_persetujuan']; ?></td>
                                <td><?= $t['harga']; ?></td>
                                <td><?= $t['subtotal']; ?></td>
                                <td>
                                    <a href=""><i class="bi bi-trash3-fill"></i></a>
                                </td>
                            </tr>
                            <?php $i++ ?>
                            <?php array_push($array_harga, $t['subtotal']); ?>
                        <?php endforeach; ?>
                        <?php $total = array_sum($array_harga); ?>
                    </tbody>
                </table>
            </div>
            <div class="d-grid col-2 mx-auto pb-3">
                <a href="/form_addbarang"><button type="button" class="btn btn-outline-primary btn-sm rounded-pill text-dark fw-bold "><i class="bi bi-plus-circle-fill pe-1"></i>Baris Baru</button></a>
            </div>
            <div class="row ps-4 mt-4">
                <div class="col-md-6 pt-3">
                    <div class="d-flex">Total Keseluruhan
                        <div class="col-md-6 ms-1 border bg-light rounded-pill ket">
                            <div class="ms-2"> <?= $total; ?> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-toolbar pt-4 mt-5 justify-content-end" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group me-4" role="group" aria-label="Second group">
                    <a href="#"><button type="submit" class="btn btn-success rounded-pill fw-bold">Approve</button></a>
                </div>
                <div class="btn-group me-4" role="group" aria-label="Second group">
                    <a href="#"><button type="submit" class="btn btn-danger rounded-pill fw-bold">Cancel</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>