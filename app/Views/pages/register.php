<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <script src="https://kit.fontawesome.com/d10c4eb8a0.js" crossorigin="anonymous"></script>
    <title>Review Data Barang</title>
</head>

<body>
    <div class="cover" style="background-image:url(img/bg-login.jpg);
    background-size: cover;
    height: 100vh;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5 position-absolute top-50 start-50 translate-middle">
                    <div class="card w-20rem card-login">
                        <div class="card-header bg-transparent mb-0">
                            <h3 class="text-center text-light">Register</h3>
                        </div>
                        <div class="card-body">
                            <form class="row g-3">
                                <div class="col-12">
                                    <i class="bi bi-envelope-fill position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <input type="email" class="form-control ps-5" id="inputEmail" placeholder="email">
                                </div>
                                <div class="col-12">
                                    <i class="bi bi-person-circle position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <input type="text" class="form-control ps-5" id="inputUsername" placeholder="username">
                                </div>
                                <div class="col-12">
                                    <i class="bi bi-lock-fill position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <input type="password" class="form-control ps-5" id="inputPassword" placeholder="password">
                                </div>
                                <div class="col-md-6">
                                    <i class="bi bi-person-fill position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <input type="text" class="form-control ps-5" id="inputNama" placeholder="nama">
                                </div>
                                <div class="col-md-6">
                                    <i class="bi bi-postcard-fill position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <input type="text" class="form-control ps-5" id="inputNIP" placeholder="NIP">
                                </div>
                                <div class="col-md-6">
                                    <i class="bi bi-people-fill position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <select id="inputGender" class="form-select ps-5">
                                        <option selected>Jenis Kelamin</option>
                                        <option>Laki-laki</option>
                                        <option>Perempuan</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <i class="bi bi-telephone-fill position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <input type="text" class="form-control ps-5" id="inputTelp" placeholder="no.telp">
                                </div>
                                <div class="col-md-6">
                                    <i class="bi bi-house-fill position-absolute ps-2" style="font-size: 1.5rem;"></i>
                                    <select id="inputInstansi" class="form-select ps-5">
                                        <option selected>Instansi</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                        <option>13</option>
                                        <option>14</option>
                                        <option>15</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary rounded-pill bg-btn-auth">Create Account</button>
                                </div>
                                <div class="text-center border-top border-secondary mt-3 text-light">Sudah punya akun?
                                    <a href="/login" class="text-light">Login!</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
</body>

</html>