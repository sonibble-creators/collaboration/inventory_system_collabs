<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container mt-5">
    <table class="table table-bordered">
        <p class="card-text mt-3 fw-bold">Daftar Approved (Revised)</p>
        <thead>
            <tr class="text-center">
                <th scope="col">No</th>
                <th scope="col">Email</th>
                <th scope="col">Username</th>
                <th scope="col">Pssword</th>
                <th scope="col">Nama</th>
                <th scope="col">NIP</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">No.Telp</th>
                <th scope="col">Instansi</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <tr class="text-center">
                <th scope="row">1</th>
                <td>abc123@gmail.com</td>
                <td>abc</td>
                <td>1234567</td>
                <td>abcdefg</td>
                <td>1122334455</td>
                <td>L</td>
                <td>08745678928</td>
                <td>Kominfosan</td>
                <td class="text-warning fw-bold">Approved (Revised)</td>
            </tr>
            <tr class="text-center">
                <th scope="row">2</th>
                <td>abc123@gmail.com</td>
                <td>abc</td>
                <td>1234567</td>
                <td>abcdefg</td>
                <td>1122334455</td>
                <td>L</td>
                <td>08745678928</td>
                <td>Dinas Kesehatan</td>
                <td class="text-warning fw-bold">Approved (Revised)</td>
            </tr>
            <tr class="text-center">
                <th scope="row">3</th>
                <td>abc123@gmail.com</td>
                <td>abc</td>
                <td>1234567</td>
                <td>abcdefg</td>
                <td>1122334455</td>
                <td>L</td>
                <td>08745678928</td>
                <td>Kominfosan</td>
                <td class="text-warning fw-bold">Approved (Revised)</td>
            </tr>
            <tr class="text-center">
                <th scope="row">4</th>
                <td>abc123@gmail.com</td>
                <td>abc</td>
                <td>1234567</td>
                <td>abcdefg</td>
                <td>1122334455</td>
                <td>L</td>
                <td>08745678928</td>
                <td>Dinas Kesehatan</td>
                <td class="text-warning fw-bold">Approved (Revised)</td>
            </tr>
        </tbody>
    </table>
</div>

<?= $this->endSection(); ?>