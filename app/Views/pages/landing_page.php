<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <script src="https://kit.fontawesome.com/d10c4eb8a0.js" crossorigin="anonymous"></script>
    <title>Review Data Barang</title>
</head>

<body>
    <div class="gradasi">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col">
                <div class="card col-md-2 bg-transparent border-0 position-imglp">
                    <img src="/img/LandingPage.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col">
                <div class="card bg-transparent border-0 position-fontlp">
                    <img src="/img/font_LP.png" class="card-img-top" alt="...">
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end pt-5 mt-3">
                        <button type="button" class="btn btn-primary btn-lg bg-btn rounded-pill text-light border border-1 fw-bold fs-6">Register</button>
                        <button type="button" class="btn btn-secondary btn-lg rounded-pill text-light border border-1 fw-bold fs-6">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
</body>

</html>