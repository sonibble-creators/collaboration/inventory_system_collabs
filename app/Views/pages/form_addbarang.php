<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container mt-5 pt-5 custom-margin">
  <div class="row justify-content-center">
    <div class="col-md-8 border fw-bold fs-3">
      <h4 class="mt-3">Tambah Barang</h4>

      <!-- create data for barang -->
      <!-- will bring to create one barang into request `/pengajuan/tambah` -->
      <form action="<?php $request != null ? site_url("/pengajuan/" + $request["id_request"] + "/edit") : site_url("/pengajuan/tambah") ?>" method="post">
        <?= csrf_field(); ?>
        <div class="container">
          <div class="row row-cols-2 pt-4">
            <div class="mb-3">
              <label for="formGroupExampleInput" class="form-label">
                <h6>Nama Barang</h6>
              </label>
              <input type="text" name="nama_barang" class="form-control" id="formGroupExampleInput" placeholder="Nama Barang">
            </div>
            <div class="mb-3">
              <label for="formGroupExampleInput" class="form-label">
                <h6>Jumlah Permintaan</h6>
              </label>
              <input type="number" name="jumlah_permintaan" class="form-control" id="formGroupExampleInput" placeholder="Banyak Barang">
            </div>
            <div class="mb-3">
              <label for="formGroupExampleInput" class="form-label">
                <h6>Tanggal Permintaan</h6>
              </label>
              <input type="date" name="tanggal_permintaan" class="form-control" id="formGroupExampleInput" placeholder="Tanggal Masuk">
            </div>
            <div class="mt-4 text-end p-2">
              <input type="submit" class="btn bg-btn rounded-pill text-light"></input>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?= $this->endSection(); ?>