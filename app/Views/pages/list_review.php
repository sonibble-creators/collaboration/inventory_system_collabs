<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<div class="container mt-5">
    <table class="table table-bordered">
        <p class="card-text mt-3 fw-bold">Daftar Review</p>
        <thead>
            <tr class="text-center">
                <th scope="col">No</th>
                <th scope="col">id transaksi</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Nama User</th>
                <th scope="col">Instansi</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <tr class="text-center">
                <th scope="row">1</th>
                <td>112233</td>
                <td>2-2-2022</td>
                <td>Kadek Abc</td>
                <td>Kominfosan</td>
                <td class="text-success fw-bold">
                    <a class="link-primary col" href="#">Detail</a> |
                    <a class="link-danger col" href="#">Delete</a>
                </td>

            </tr>
            <tr class="text-center">
                <th scope="row">2</th>
                <td>112233</td>
                <td>2-2-2022</td>
                <td>Kadek Abc</td>
                <td>Dinas Kesehatan</td>
                <td class="text-success fw-bold">
                    <a class="link-primary col" href="#">Detail</a> |
                    <a class="link-danger col" href="#">Delete</a>
                </td>
            </tr>
            <tr class="text-center">
                <th scope="row">3</th>
                <td>112233</td>
                <td>2-2-2022</td>
                <td>Kadek Abc</td>
                <td>Kominfosan</td>
                <td class="text-success fw-bold">
                    <a class="link-primary col" href="#">Detail</a> |
                    <a class="link-danger col" href="#">Delete</a>
                </td>
            </tr>
            <tr class="text-center">
                <th scope="row">4</th>
                <td>112233</td>
                <td>2-2-2022</td>
                <td>Kadek Abc</td>
                <td>Dinas Kesehatan</td>
                <td class="text-success fw-bold">
                    <a class="link-primary col" href="#">Detail</a> |
                    <a class="link-danger col" href="#">Delete</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<?= $this->endSection(); ?>