<?php

namespace App\Models;

// use CodeIgniter\Database\Query;
use CodeIgniter\Model;

class TransaksiModel extends Model
{
  protected $table            = 'transaksi';
  protected $primaryKey       = 'id_transaksi';
  protected $allowedFields    = ['id_barang', 'tgl_pengajuan', 'tgl_persetujuan', 'approval'];
  protected $useTimestamps    = true;
  protected $dateFormat       = 'date';
  protected $useAutoIncrement = true;

  function findAllData()
  {
    $builder = $this->db->table("transaksi");
    $builder->select('barang.nama_barang, barang.harga, transaksi.*, request.jumlah_permintaan, (barang.harga * request.jumlah_permintaan) as subtotal');
    $builder->join('barang', 'transaksi.id_barang = barang.id_barang');
    $builder->join('request', 'barang.id_barang = request.id_barang');
    $builder->where('transaksi.approval', '1');
    return $builder->get()->getResult('array');
  }
}
