<?php

namespace App\Models;

use CodeIgniter\Model;

class RequestModel extends Model
{
  protected $table = 'request';
  protected $primaryKey = 'id_permintaan';
  protected $useAutoIncrement = true;
  protected $useTimestamps = true;
  protected $dateFormat = 'date';
  protected $allowedFields = [
    "nama_barang",
    "jumlah_permintaan",
    "id_barang",
    "tanggal_permintaan",
    "id_user"
  ];
}
