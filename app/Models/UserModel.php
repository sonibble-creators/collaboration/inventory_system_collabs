<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
  protected $table            = 'user';
  protected $primaryKey       = 'id_user';
  protected $useTimestamps    = true;
  protected $dateFormat       = 'date';
  protected $useAutoIncrement = true;
  protected $allowedFields = [
    "username",
    "password",
    "instansi",
    "jenis_kelamin",
    "no_telp",
    "email",
    "NIP",
    "role",
    "nama"
  ];
}
