<?php

namespace App\Models;

use CodeIgniter\Model;

class BarangModel extends Model
{
  protected $table            = 'barang';
  protected $primaryKey       = 'id_barang';
  protected $useAutoIncrement = true;
  protected $useTimestamps    = true;
  protected $dateFormat       = 'date';
  protected $allowedFields    = [
    "nama_barang",
    "stok_barang",
    "tgl_masuk",
    "harga",
  ];
}
